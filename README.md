# laravel版本更新指南 #
laravel版本从`5.1`升级到`5.5`详细更新步骤

## 从5.1.x升级到5.2.x ##

### 更新依赖 ###
更新 `composer.json` 文件指向 `laravel/framework 5.2.*`。注：如果你安装的是 `Laravel 5.2` 的 `beta` 版本，还要添加 `"minimum-stability": "beta"` 到 `composer.json` 文件。添加 `symfony/dom-crawler ~3.0` 和 `symfony/css-selector ~3.0` 到 `composer.json` 的 `require-dev` 部分

### 更新认证配置文件 ###
更新 `config/auth.php` 文件内容如下：[https://github.com/laravel/laravel/blob/develop/config/auth.php](https://github.com/laravel/laravel/blob/develop/config/auth.php)  

    <?php
    return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Defaults
    |--------------------------------------------------------------------------
    |
    | This option controls the default authentication "guard" and password
    | reset options for your application. You may change these defaults
    | as required, but they're a perfect start for most applications.
    |
    */
    'defaults' => [
        'guard' => 'web',
        'passwords' => 'users',
    ],
    /*
    |--------------------------------------------------------------------------
    | Authentication Guards
    |--------------------------------------------------------------------------
    |
    | Next, you may define every authentication guard for your application.
    | Of course, a great default configuration has been defined for you
    | here which uses session storage and the Eloquent user provider.
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | Supported: "session", "token"
    |
    */
    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],
        'api' => [
            'driver' => 'token',
            'provider' => 'users',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | User Providers
    |--------------------------------------------------------------------------
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | If you have multiple user tables or models you may configure multiple
    | sources which represent each model / table. These sources may then
    | be assigned to any extra authentication guards you have defined.
    |
    | Supported: "database", "eloquent"
    |
    */
    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => App\User::class,
        ],
        // 'users' => [
        //     'driver' => 'database',
        //     'table' => 'users',
        // ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Resetting Passwords
    |--------------------------------------------------------------------------
    |
    | You may specify multiple password reset configurations if you have more
    | than one user table or model in the application and you want to have
    | separate password reset settings based on the specific user types.
    |
    | The expire time is the number of minutes that the reset token should be
    | considered valid. This security feature keeps tokens short-lived so
    | they have less time to be guessed. You may change this as needed.
    |
    */
    'passwords' => [
        'users' => [
            'provider' => 'users',
            'table' => 'password_resets',
            'expire' => 60,
                ],
            ],
                ];

### 移除服务提供者 ###
`Illuminate\Foundation\Providers\ArtisanServiceProvider` 从配置文件 `config/app.php` 的服务提供者列表中移除。
`Illuminate\Routing\ControllerServiceProvider` 从配置文件 `config/app.php` 的服务提供者列表中移除。

### 完成更新 ###
    composer update

### 更多说明参见 ###
[从laravel 5.1.x到5.2.x](https://laravelacademy.org/post/2666.html) 

## 从5.2.x升级到5.3.x ##

### 更新依赖 ###
更新`composer.json`中对应的`laravel/framework`到`5.3.*`以及`symfony/css-selector`和`symfony/dom-crawler`到`3.1.*`(require-dev下)

### 更新认证脚手架 ###
框架提供的默认的两个认证控制器已经被分割成四个,最简单的方法是从github中把4个控制器的代码拷贝到项目中去，[https://github.com/laravel/laravel/tree/master/app/Http/Controllers/Auth](https://github.com/laravel/laravel/tree/master/app/Http/Controllers/Auth)。

### 合并Trait ###
`AuthorizesResourcestrait`已经和`AuthorizesRequeststrait`合并到一起，需要从`app/Http/Controllers/Controller.php`中移除`AuthorizesResourcestrait`。

### 添加广播(通知) ###
，需要添加的新的`BroadcastServiceProvider`[从GitHub下载文件](https://raw.githubusercontent.com/laravel/laravel/develop/app/Providers/BroadcastServiceProvider.php)到`app/Providers`目录，然后将这个新的服务提供者注册到配置文件`config/app.php`的`providers`数组中,配置如下:  

	 Illuminate\Notifications\NotificationServiceProvider::class  

还需要在配置文件`config/app.php`的`aliases`数组中注册门面`Illuminate\Support\Facades\Notification`:  

	'Notification' =>Illuminate\Support\Facades\Notification::class  
	
最后，你可以在`User`模型或其它你希望接收通知的模型中使用 `Illuminate\Notifications\Notifiable trait`。	 

### 更新中间件 ###
can中间件命名空间修改,罗列在`Http`文件夹下的 `Kernel.php`的`$routeMiddleware`属性中的`can中间件`需要作如下修改:  

	'can' => \Illuminate\Auth\Middleware\Authorize::class  
	
路由模型绑定现在通过中间件来完成，所有应用都需要在a`pp/Http/Kernel.php`文件的web中间件组中添加`Illuminate\Routing\Middleware\SubstituteBindings`:  

	\Illuminate\Routing\Middleware\SubstituteBindings::class  
	
还需要在`HTTP Kernel`的`$routeMiddleware`属性中注册路由中间件用于绑定替代:  

	'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class  
	
这个路由中间件注册后，还要将其添加到api中间件组：  

	'api' => [
    'throttle:60,1',
    'bindings',
    ],

### 更新路由 ###
之前版本的Laravel中，使用`Route::resource`注册的路由参数并没有“单数化”，这可能会在注册路由模型绑定的时候引起一些非预期的行为,而在Laravel 5.3中，所有资源路由参数默认都是单数化的。如果你想要继续维护之前版本的行为而不是自动单数化资源路由参数，可以在`AppServiceProvider`中这样调用`singularResourceParameters`方法:  

	use Illuminate\Support\Facades\Route;

	Route::singularResourceParameters(false);  
	
使用`Route::resource`的时候URI前缀将不再影响分配给路由的路由名称，如果在`Route::group`中使用`Route::resource`时调用了指定的`prefix`选项，则需要检查所有对route辅助函数的调用以验证不再追加URI的prefix到路由名称。如果这一改动导致同一名称下有两个路由，可以在调用`Route::resource`的时候使用names选项为给定路由指定一个自定义路由。  
在 5.3 中，`app/Http/routes.php` 文件被移到了 `routes` 目录下，并且被分割成两个文件：`web.php`和 `api.php`。你可能已经猜到了，`web.php` 中的路由应用了 web 中间件组，而 `api.php` 中的路由应用了api 中间件组。如果你想要自定义自己的独立路由文件，可参考 `App\Providers\RouteServiceProvider` 文件:

	 public function map()
     {
        $this->mapWebRoutes();

        $this->mapApiRoutes();

        //
     }

    protected function mapWebRoutes()
    {
        Route::group([
            'namespace' => $this->namespace, 'middleware' => 'web',
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }

    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => ['api', 'auth:api'],
            'namespace' => $this->namespace,
            'prefix' => 'api',
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }  
	
这样就可以非常简单的创建自己的路由文件了。

### 注意及备注 ###  
在完成上面的步骤后还需要使用命令`composer require pusher/pusher-php-server`以及`composer update`还有`composer dump-autoload`完成应用更新。将`pusher`加入`config/app.php`配置文件的`aliases`数组下:  

	'Pusher' =>\Pusher\Pusher::class  

接下来使用以下命令清除缓存:  

	php artisan route:cache
	php artisan view:clear
	php artisan config:clear
	php artisan cache:clear
	php artisan clear-compiled
	php artisan optimize
	php artisan route:cache  

最后使用`composer update`完成更新。

### 更多说明 ###
[从Laravel 5.2.x升级到5.3.x](https://laravelacademy.org/post/5691.html)

## 从5.3.x升级到5.4.x ##

### 升级依赖 ###
将`composer.json`文件中的`laravel/framework`依赖升级到`5.4.*`。此外，还需要更新`phpunit/phpunit`依赖的版本为`~5.7`。接下来使用命令:  

	composer update  
	
完成更新。

### 清理缓存 ###
升级完所有包之后，需要运行`php artisan view:clear`来避免移除`Illuminate\View\Factory::getFirstLoop()`引起的`Blade`错误，此外，还需要运行`php artisan route:clear`来清空路由缓存。

### 继续使用Tinker ###
为了可以在`Artisan`命令中继续使用`tinker`命令，需要安装`laravel/tinker`扩展包:  

	composer require laravel/tinker  
	
安装完成后，需要在`config/app.php`的`providers`数组中注册`Laravel\Tinker\TinkerServiceProvider::class`。

### 更多说明 ###
[从Laravel 5.3.x升级到5.4.x](http://laravelacademy.org/post/6658.html)

